import epanet_fiware.epanetmodel
import epanet_fiware.enumerations as enu
import epanet_fiware.epanet_outfile_handler as outfile_handler
import epanet.toolkit as en
import pandas as pd
import numpy as np
import datetime
from typing import Optional
#%%
class IMM_model:
    def __init__(self,
                 inp_file: str,
                 network_name: str,
                 sensors: list
                 ):
        self.inp_file = inp_file
        self.network_name = network_name
        self.epanetmodel = None
        self.sensors = sensors
        self.simulationParameters = {}
        self.PI_results ={}
        self.baseDemand_m3 = None
        self.baseDRscore = None
        self.node_pop = None

    def set_simulation_Parameters(self,
                                  awarenesstime: Optional[datetime.datetime] = datetime.datetime.now(),
                                  hyd_time_step: Optional[int] = 900,
                                  pmin: Optional[int] = 3,
                                  preq: Optional[int] = 15,
                                  leakEmitter: Optional[int] = 50,
                                  leakExponent: Optional[float] = 0.99,
                                  repair_sec: Optional[int] = 4*60*60, #4 hours
                                  leakNodeID = None,
                                  leakPipeID = None,
                                  OperationalPipeIDs = None,
                                  leakStartTime = None
                                  ):

        awarenesstime = awarenesstime.replace(minute=0, second=0, microsecond=0) + datetime.timedelta(hours=1)  # rounds up to next hour
        patternStartday = datetime.date(2022, 1, 1).weekday()  # simulations must start on 5th weekday (i.e.sat)
        if awarenesstime.weekday() > patternStartday:
            simulation_start_date = (awarenesstime - datetime.timedelta(days=awarenesstime.weekday()) + datetime.timedelta(days=5))
        else:
            simulation_start_date = (awarenesstime - datetime.timedelta(days=awarenesstime.weekday()) + datetime.timedelta(days=5,
                                                                                                          weeks=-1))
        if leakStartTime == None:
            leakStartTime = awarenesstime - datetime.timedelta(hours=2)

        simulation_start_date = simulation_start_date.replace(hour=0)
        self.simulationParameters['awarenesstime'] = awarenesstime
        self.simulationParameters['leak_start_date'] = leakStartTime
        self.simulationParameters['simulation_start_date'] =  simulation_start_date
        self.simulationParameters['simulation_end_date'] = awarenesstime+datetime.timedelta(days=2)
        self.simulationParameters['hyd_time_step'] = hyd_time_step
        self.simulationParameters['Pmin'] = pmin
        self.simulationParameters['Preq'] = preq
        self.simulationParameters['leakNodeID'] = leakNodeID
        self.simulationParameters['leakPipeID'] = leakPipeID
        self.simulationParameters['repair_sec'] = repair_sec
        self.simulationParameters['leakEmitter'] = leakEmitter
        self.simulationParameters['leakExponent'] = leakExponent
        self.simulationParameters['OperationalPipeIDs'] = OperationalPipeIDs

    def load_epanetmodel(self):
        self.epanetmodel = epanet_fiware.epanetmodel.EPAnetModel(self.network_name, self.inp_file)

    def get_sensor_indices(self):
        self.load_epanetmodel()
        for i in range(len(self.sensors)):
            if self.sensors[i]['Type'] == 'pressure':
                self.sensors[i]['Index'] = en.getnodeindex(self.epanetmodel.proj_for_simulation, self.sensors[i]['ID'])
            if self.sensors[i]['Type'] == 'flow':
                self.sensors[i]['Index'] = en.getlinkindex(self.epanetmodel.proj_for_simulation, self.sensors[i]['ID'])
        self.epanetmodel = None

    def get_sensor_data(self, report_steps, stepDuration, simulation_date):
        rows = []
        for sensor in self.sensors:
            if sensor['Type'] == 'pressure':
                for report_step in range(report_steps):
                    sensor_type = sensor['Type']
                    read = self.epanetmodel.get_node_result(report_step, outfile_handler.NodeResultLabels.Pressure,
                                                      node_id=sensor['ID'])
                    report_time = simulation_date + datetime.timedelta(0, report_step * stepDuration)
                    rows.append([report_step, report_time, sensor['ID'], sensor_type, read])
            if sensor['Type'] == 'flow':
                for report_step in range(report_steps):
                    sensor_type = sensor['Type']
                    read = self.epanetmodel.get_link_result(report_step, outfile_handler.LinkResultLabels.Flow,
                                                      link_id=sensor['ID'])
                    report_time = simulation_date + datetime.timedelta(0, report_step * stepDuration)
                    rows.append([report_step, report_time, sensor['ID'], sensor_type, read])

        df = pd.DataFrame(rows, columns=['ReportStep', 'ReportTime', 'Sensor_ID', 'Sensor_type', 'Read'])
        return df

    def get_pop_per_node(self, total_population: Optional[int] = 1000, pop_per_node_list: Optional = False):
        if pop_per_node_list is False:# if not provided calc weighted avg based of demand
            self.load_epanetmodel()
            nodeIDs = self.epanetmodel.get_node_ids(enu.NodeTypes.Junction)
            nodes = []
            total_base_demand = 0
            for nodeID in nodeIDs:
                node_index = en.getnodeindex(self.epanetmodel.proj_for_simulation, nodeID)
                num_demands = en.getnumdemands(self.epanetmodel.proj_for_simulation, node_index)
                base_demand = 0
                for demand_index in range(1, num_demands):
                    base_demand = base_demand + en.getbasedemand(self.epanetmodel.proj_for_simulation, node_index,
                                                                 demand_index)
                node_dict = {}
                node_dict['ID'] = nodeID
                node_dict['index'] = node_index
                node_dict['base_demand'] = base_demand
                nodes.append(node_dict)
                total_base_demand = total_base_demand + base_demand
            for node in nodes:
                node['pop'] = round(node['base_demand'] * total_population / total_base_demand)
            self.node_pop = nodes
            self.epanetmodel = None

    def base_scenario_sim(self):
        self.load_epanetmodel()
        duration = int((self.simulationParameters['simulation_end_date'] - self.simulationParameters['simulation_start_date']).total_seconds())
        stepDuration = self.simulationParameters['hyd_time_step']
        awareness_sec = int((self.simulationParameters['awarenesstime'] - self.simulationParameters['simulation_start_date']).total_seconds())
        #used to calculate basedemand & baseDRscore
        self.epanetmodel.set_time_param(enu.TimeParams.Duration, (duration))  # set simulation duration for 15 days
        self.epanetmodel.set_time_param(enu.TimeParams.HydStep, stepDuration)  # set hydraulic time step to 15min
        self.epanetmodel.set_time_param(enu.TimeParams.ReportStep, stepDuration)  # set reporting time step to 15min
        self.epanetmodel.set_epanet_mode(enu.EpanetModes.PDA, pmin=self.simulationParameters['Pmin'], preq=self.simulationParameters['Preq'], pexp=0.5)  # set demand mode
        # run simulation step-by-step
        en.openH(self.epanetmodel.proj_for_simulation)
        en.initH(self.epanetmodel.proj_for_simulation, en.NOSAVE)
        en.setoption(self.epanetmodel.proj_for_simulation, en.TRIALS, 100)  # reduce number of trials for convergence
        en.setoption(self.epanetmodel.proj_for_simulation, en.ACCURACY,0.01)  # reduce accuracy required for convergence
        en.setoption(self.epanetmodel.proj_for_simulation, en.EMITEXPON, self.simulationParameters['leakExponent'])


        t = en.nextH(self.epanetmodel.proj_for_simulation)
        pipes = self.create_pipeDict()
        self.baseDemand_m3 = 0
        self.baseDRscore = 0


        while t > 0:
            en.runH(self.epanetmodel.proj_for_simulation)
            hyd_sim_seconds = en.gettimeparam(self.epanetmodel.proj_for_simulation, en.HTIME)
            report_step = hyd_sim_seconds / stepDuration
            # calc basedemand and basevelocities after awarenesstime (i.e. last 48 hr of simulation)
            if hyd_sim_seconds>=awareness_sec:
                baseDemand_lps = 0
                for node in self.node_pop:
                    baseDemand_lps = baseDemand_lps + en.getnodevalue(self.epanetmodel.proj_for_simulation, node['index'],en.DEMAND)
                self.baseDemand_m3 = self.baseDemand_m3 + (baseDemand_lps*stepDuration/1000)

                for pipe in pipes:
                    baseVelocity = en.getlinkvalue(self.epanetmodel.proj_for_simulation, pipe['index'], en.VELOCITY)
                    if baseVelocity > pipe['max_velocity']:
                        pipe['max_velocity'] = baseVelocity

            t = en.nextH(self.epanetmodel.proj_for_simulation)
        self.epanetmodel = None

        for pipe in pipes:
            if pipe['max_velocity'] < 0.4:
                self.baseDRscore = self.baseDRscore + 1
            if 0.4 <= pipe['max_velocity'] < 0.8:
                self.baseDRscore = self.baseDRscore + 2
            if 0.8 <= pipe['max_velocity'] < 1.2:
                self.baseDRscore = self.baseDRscore + 3
            if 1.2 <= pipe['max_velocity'] < 1.6:
                self.baseDRscore = self.baseDRscore + 4
            if pipe['max_velocity'] >= 1.6:
                self.baseDRscore = self.baseDRscore + 5

    def default_repair_scenario(self):
        duration = int((self.simulationParameters['simulation_end_date'] - self.simulationParameters['simulation_start_date']).total_seconds())
        stepDuration = self.simulationParameters['hyd_time_step']
        awareness_sec = int((self.simulationParameters['awarenesstime'] - self.simulationParameters['simulation_start_date']).total_seconds())
        repair_start_sec = awareness_sec
        repair_end_sec = repair_start_sec + self.simulationParameters['repair_sec']
        leak_start_sec = int((self.simulationParameters['leak_start_date'] - self.simulationParameters['simulation_start_date']).total_seconds())
        self.load_epanetmodel()
        #start_time = datetime.datetime.now()
        leakIndex = en.getnodeindex(self.epanetmodel.proj_for_simulation, self.simulationParameters['leakNodeID'])
        pipeIndex = en.getlinkindex(self.epanetmodel.proj_for_simulation, self.simulationParameters['leakPipeID'])
        self.epanetmodel.set_time_param(enu.TimeParams.Duration, (duration))  # set simulation duration for 15 days
        self.epanetmodel.set_time_param(enu.TimeParams.HydStep, stepDuration)  # set hydraulic time step to 15min
        self.epanetmodel.set_time_param(enu.TimeParams.ReportStep, stepDuration)  # set reporting time step to 15min
        self.epanetmodel.set_epanet_mode(enu.EpanetModes.PDA, pmin=self.simulationParameters['Pmin'], preq=self.simulationParameters['Preq'], pexp=0.5)  # set demand mode

        # run simulation step-by-step
        en.openH(self.epanetmodel.proj_for_simulation)
        en.initH(self.epanetmodel.proj_for_simulation, en.NOSAVE)
        en.setoption(self.epanetmodel.proj_for_simulation, en.EMITEXPON, self.simulationParameters['leakExponent'])
        en.setoption(self.epanetmodel.proj_for_simulation, en.TRIALS, 100)  # reduce number of trials for convergence
        en.setoption(self.epanetmodel.proj_for_simulation, en.ACCURACY,0.01)  # reduce accuracy required for convergence

        t = en.nextH(self.epanetmodel.proj_for_simulation)

        PIs = {'PI1': 0, 'PI2': 0, 'PI3': 0, 'PI4': 0}
        leakDemand_m3 = 0
        leakvolume = 0
        newDRscore = 0
        pipes = self.create_pipeDict()

        while t > 0:
            en.runH(self.epanetmodel.proj_for_simulation)
            hyd_sim_seconds = en.gettimeparam(self.epanetmodel.proj_for_simulation, en.HTIME)
            report_step = hyd_sim_seconds / stepDuration
            if report_step == int(leak_start_sec/stepDuration):  # add orifice leak
                en.setnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.EMITTER, self.simulationParameters['leakEmitter'])
            if report_step == int(repair_start_sec/stepDuration): #stop leak, close pipe
                en.setnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.EMITTER,0.00000000001) #stop pipe leak - error with owa programming - 0 doesn't work.
                en.setlinkvalue(self.epanetmodel.proj_for_simulation, pipeIndex, en.STATUS, 0) #0 means closed, 1 means open
            if report_step == int(repair_end_sec / stepDuration): #open pipe that's been repaired
                en.setlinkvalue(self.epanetmodel.proj_for_simulation, pipeIndex, en.STATUS, 1)
            #calc PIs
            if hyd_sim_seconds >= awareness_sec:
                PI1, PI2, PI3_demand = self.PI_step()
                PIs['PI1'] = PIs['PI1'] + PI1 * stepDuration
                PIs['PI2'] = PIs['PI2'] + PI2 * stepDuration
                leakDemand_m3 = leakDemand_m3 + (PI3_demand * stepDuration / 1000)

            # calc leakage volume while leak is running
            if (hyd_sim_seconds >= awareness_sec) & (hyd_sim_seconds <= repair_start_sec):
                leak_pressure = en.getnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.PRESSURE)
                leakflow = self.simulationParameters['leakEmitter'] * (leak_pressure ** self.simulationParameters['leakExponent'])
                leakvolume = leakvolume + (leakflow * stepDuration / 1000)

            for pipe in pipes:
                velocity = en.getlinkvalue(self.epanetmodel.proj_for_simulation, pipe['index'], en.VELOCITY)
                if velocity > pipe['max_velocity']:
                    pipe['max_velocity'] = velocity

            t = en.nextH(self.epanetmodel.proj_for_simulation)
        self.epanetmodel = None

        for pipe in pipes:
            if pipe['max_velocity'] < 0.4:
                newDRscore = newDRscore + 1
            if 0.4 <= pipe['max_velocity'] < 0.8:
                newDRscore = newDRscore + 2
            if 0.8 <= pipe['max_velocity'] < 1.2:
                newDRscore = newDRscore + 3
            if 1.2 <= pipe['max_velocity'] < 1.6:
                newDRscore = newDRscore + 4
            if pipe['max_velocity'] >= 1.6:
                newDRscore = newDRscore + 5

        print("leak Volume is " + str(leakvolume))
        PIs['PI3'] = self.baseDemand_m3 - (leakDemand_m3 - leakvolume)
        PIs['PI4'] = newDRscore - self.baseDRscore
        self.PI_results['default_repair'] = PIs
        return PIs  # , self.baseDemand_m3, leakDemand_m3, leakvolume

    def assess_IMM(self, x):
        duration = int((self.simulationParameters['simulation_end_date'] - self.simulationParameters['simulation_start_date']).total_seconds())
        stepDuration = self.simulationParameters['hyd_time_step']
        awareness_sec = int((self.simulationParameters['awarenesstime'] - self.simulationParameters['simulation_start_date']).total_seconds())
        repair_start_sec = awareness_sec + (x[0]*60*60)
        repair_end_sec = repair_start_sec + self.simulationParameters['repair_sec']
        leak_start_sec = int((self.simulationParameters['leak_start_date'] - self.simulationParameters['simulation_start_date']).total_seconds())
        self.load_epanetmodel()
        # start_time = datetime.datetime.now()
        leakIndex = en.getnodeindex(self.epanetmodel.proj_for_simulation, self.simulationParameters['leakNodeID'])
        pipeIndex = en.getlinkindex(self.epanetmodel.proj_for_simulation, self.simulationParameters['leakPipeID'])
        self.epanetmodel.set_time_param(enu.TimeParams.Duration, (duration))
        self.epanetmodel.set_time_param(enu.TimeParams.HydStep, (stepDuration))
        self.epanetmodel.set_time_param(enu.TimeParams.ReportStep, (stepDuration))
        self.epanetmodel.set_epanet_mode(enu.EpanetModes.PDA, pmin=3, preq=15, pexp=0.5)  # set demand mode
        # run simulation step-by-step
        en.openH(self.epanetmodel.proj_for_simulation)
        en.initH(self.epanetmodel.proj_for_simulation, en.NOSAVE)
        en.setoption(self.epanetmodel.proj_for_simulation, en.EMITEXPON, self.simulationParameters['leakExponent'])
        en.setoption(self.epanetmodel.proj_for_simulation, en.TRIALS, 100)  # reduce number of trials for convergence
        en.setoption(self.epanetmodel.proj_for_simulation, en.ACCURACY, 0.01)  # reduce accuracy required for convergence

        t = en.nextH(self.epanetmodel.proj_for_simulation)

        PIs = {'PI1': 0, 'PI2': 0, 'PI3': 0, 'PI4': 0}
        leakDemand_m3 = 0
        leakvolume = 0
        newDRscore = 0
        pipes = self.create_pipeDict()

        while t > 0:
            en.runH(self.epanetmodel.proj_for_simulation)
            hyd_sim_seconds = en.gettimeparam(self.epanetmodel.proj_for_simulation, en.HTIME)
            report_step = hyd_sim_seconds / stepDuration
            if report_step == int(leak_start_sec / stepDuration):  # add orifice leak
                en.setnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.EMITTER,
                                self.simulationParameters['leakEmitter'])
            if report_step == int(repair_start_sec / stepDuration):  # stop leak, close pipe for repair
                en.setnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.EMITTER, 0.00000000001)  # stop pipe leak
                en.setlinkvalue(self.epanetmodel.proj_for_simulation, pipeIndex, en.STATUS, 0)  # 0 means closed, 1 means open
            if x[1] > 0:  # operate closed pipe
                operational_pipeIndex = en.getlinkindex(self.epanetmodel.proj_for_simulation,
                                                        self.simulationParameters['OperationalPipeIDs'][0])
                operate_pipe1_topen = awareness_sec + ((x[1] - 1) * 60*60)
                opreate_pipe1_tclose = awareness_sec + ((x[2] - 1) * 60*60)
                if report_step == int(operate_pipe1_topen / stepDuration):
                    en.setlinkvalue(self.epanetmodel.proj_for_simulation, operational_pipeIndex, en.STATUS, 1)  # 0 means closed, 1 means open
                if report_step == int(opreate_pipe1_tclose / stepDuration):
                    en.setlinkvalue(self.epanetmodel.proj_for_simulation, operational_pipeIndex, en.STATUS, 0)  # 0 means closed, 1 means open

            if report_step == int(repair_end_sec / stepDuration):  # open pipe that's been repaired
                en.setlinkvalue(self.epanetmodel.proj_for_simulation, pipeIndex, en.STATUS, 1)
            # calc PIs
            if hyd_sim_seconds >= awareness_sec:
                PI1, PI2, PI3_demand = self.PI_step()
                PIs['PI1'] = PIs['PI1'] + PI1 * stepDuration
                PIs['PI2'] = PIs['PI2'] + PI2 * stepDuration
                leakDemand_m3 = leakDemand_m3 + (PI3_demand * stepDuration / 1000)

            # calc leakage volume while leak is running
            if (hyd_sim_seconds >= awareness_sec) & (hyd_sim_seconds <= repair_start_sec):
                leak_pressure = en.getnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.PRESSURE)
                leakflow = self.simulationParameters['leakEmitter'] * (
                            leak_pressure ** self.simulationParameters['leakExponent'])
                leakvolume = leakvolume + (leakflow * stepDuration / 1000)

            for pipe in pipes:
                velocity = en.getlinkvalue(self.epanetmodel.proj_for_simulation, pipe['index'], en.VELOCITY)
                if velocity > pipe['max_velocity']:
                    pipe['max_velocity'] = velocity

            t = en.nextH(self.epanetmodel.proj_for_simulation)
        self.epanetmodel = None

        for pipe in pipes:
            if pipe['max_velocity'] < 0.4:
                newDRscore = newDRscore + 1
            if 0.4 <= pipe['max_velocity'] < 0.8:
                newDRscore = newDRscore + 2
            if 0.8 <= pipe['max_velocity'] < 1.2:
                newDRscore = newDRscore + 3
            if 1.2 <= pipe['max_velocity'] < 1.6:
                newDRscore = newDRscore + 4
            if pipe['max_velocity'] >= 1.6:
                newDRscore = newDRscore + 5

        PIs['PI3'] = self.baseDemand_m3 - (leakDemand_m3 - leakvolume)
        PIs['PI4'] = newDRscore - self.baseDRscore
        if self.PI_results['default_repair']['PI1'] <=0:
            PIs['PI1_marg'] = (- PIs['PI1']) / 1
        else:
            PIs['PI1_marg'] =(self.PI_results['default_repair']['PI1'] -  PIs['PI1']) / self.PI_results['default_repair']['PI1']
        if self.PI_results['default_repair']['PI2'] <= 0:
            PIs['PI2_marg'] = (- PIs['PI2']) / 1
        else:
            PIs['PI2_marg'] = (self.PI_results['default_repair']['PI2'] - PIs['PI2']) / self.PI_results['default_repair']['PI2']
        if self.PI_results['default_repair']['PI3'] <= 0:
            PIs['PI3_marg'] = (- PIs['PI3']) / 1
        else:
            PIs['PI3_marg'] = (self.PI_results['default_repair']['PI3'] - PIs['PI3']) / self.PI_results['default_repair']['PI3']
        if self.PI_results['default_repair']['PI4'] <= 0:
            PIs['PI4_marg'] = (- PIs['PI4']) / 1
        else:
            PIs['PI4_marg'] = (self.PI_results['default_repair']['PI4'] - PIs['PI4']) / self.PI_results['default_repair']['PI4']

        Total_PI = PIs['PI1_marg']*5 +  PIs['PI2_marg']*2 + PIs['PI3_marg']*2+ PIs['PI4_marg']*1 #objctive maximize total_PI
        print("leak Volume is " + str(leakvolume))
        print("PI_1 = "+str(PIs['PI1']) + " Default PI_1 = " + str(self.PI_results['default_repair']['PI1']) + " Marginalized PI_1 = " + str(PIs['PI1_marg']))
        print("PI_2 = "+str(PIs['PI2']) + " Default PI_2 = " + str(self.PI_results['default_repair']['PI2']) + " Marginalized PI_2 = " + str(PIs['PI2_marg']))
        print("PI_3 = "+str(PIs['PI3']) + " Default PI_3 = " + str(self.PI_results['default_repair']['PI3']) + " Marginalized PI_3 = " + str(PIs['PI3_marg']))
        print("PI_4 = "+str(PIs['PI4']) + " Default PI_4 = " + str(self.PI_results['default_repair']['PI4']) + " Marginalized PI_4 = " + str(PIs['PI4_marg']))
        print("Final obj Total PI is " + str(Total_PI))
        return Total_PI


    def create_pipeDict(self):
        pipes = en.getcount(self.epanetmodel.proj_for_simulation, en.LINKCOUNT) - 1
        pipe_list = []
        for pipe in range(1, pipes):
            pipe_dict = {}
            pipe_dict['index'] = pipe
            pipe_dict['max_velocity'] = 0
            pipe_dict['DR_BaseScore'] = 0
            pipe_list.append(pipe_dict)
        return pipe_list

    def calc_PIs(self,
                 duration: int,
                 stepDuration: int,
                 leakID: str,
                 leakEmitter: float,
                 leakStart_step: int,
                 leakExponent: Optional[float] = 0.99
                       ):
        self.load_epanetmodel()
        start_time = datetime.datetime.now()
        leakIndex = en.getnodeindex(self.epanetmodel.proj_for_simulation, leakID)
        self.epanetmodel.set_time_param(enu.TimeParams.Duration, (duration))  # set simulation duration for 15 days
        self.epanetmodel.set_time_param(enu.TimeParams.HydStep, (stepDuration))  # set hydraulic time step to 15min
        self.epanetmodel.set_time_param(enu.TimeParams.ReportStep, (stepDuration))  # set reporting time step to 15min
        self.epanetmodel.set_epanet_mode(enu.EpanetModes.PDA, pmin=3, preq=15, pexp=0.5)  # set demand mode
        # run simulation step-by-step
        en.openH(self.epanetmodel.proj_for_simulation)
        en.initH(self.epanetmodel.proj_for_simulation, en.NOSAVE)
        en.setoption(self.epanetmodel.proj_for_simulation, en.EMITEXPON, leakExponent)
        en.setoption(self.epanetmodel.proj_for_simulation, en.TRIALS, 100) #reduce number of trials for convergence
        en.setoption(self.epanetmodel.proj_for_simulation, en.ACCURACY, 0.01) #reduce accuracy required for convergence

        t = en.nextH(self.epanetmodel.proj_for_simulation)

        PIs = {'PI1':0, 'PI2':0, 'PI3':0, 'PI4':0}
        leakDemand_m3 = 0
        leakvolume = 0
        newDRscore = 0
        pipes = self.create_pipeDict()


        while t > 0:
            hyd_sim_seconds = en.gettimeparam(self.epanetmodel.proj_for_simulation, en.HTIME)
            report_step = hyd_sim_seconds / stepDuration
            if report_step == (leakStart_step): # add orifice leak
                en.setnodevalue(self.epanetmodel.proj_for_simulation, leakIndex, en.EMITTER, leakEmitter)
            en.runH(self.epanetmodel.proj_for_simulation)
            hyd_sim_seconds = en.gettimeparam(self.epanetmodel.proj_for_simulation, en.HTIME)
            report_step = hyd_sim_seconds / stepDuration
            #calc PIs
            PI1, PI2, PI3_demand = self.PI_step()
            PIs['PI1'] = PIs['PI1'] + PI1*stepDuration
            PIs['PI2'] = PIs['PI2'] + PI2*stepDuration
            leakDemand_m3 = leakDemand_m3+(PI3_demand*stepDuration/1000)

            #calc leakage volume
            if report_step >= (leakStart_step): #& report_step < leakEnd_step:
                leak_pressure = en.getnodevalue(self.epanetmodel.proj_for_simulation, leakIndex,en.PRESSURE)
                leakflow = leakEmitter*(leak_pressure**leakExponent)
                leakvolume = leakvolume + (leakflow*stepDuration/1000)

            for pipe in pipes:
                velocity = en.getlinkvalue(self.epanetmodel.proj_for_simulation, pipe['index'], en.VELOCITY)
                if velocity > pipe['max_velocity']:
                    pipe['max_velocity'] = velocity

            t = en.nextH(self.epanetmodel.proj_for_simulation)
        for pipe in pipes:
            if pipe['max_velocity'] < 0.4:
                newDRscore = newDRscore + 1
            if 0.4 <= pipe['max_velocity'] < 0.8:
                newDRscore = newDRscore + 2
            if 0.8 <= pipe['max_velocity'] < 1.2:
                newDRscore = newDRscore + 3
            if 1.2 <= pipe['max_velocity'] < 1.6:
                newDRscore = newDRscore + 4
            if pipe['max_velocity'] >= 1.6:
                newDRscore = newDRscore + 5

        PIs['PI3'] = self.baseDemand_m3 - (leakDemand_m3 - leakvolume)
        PIs['PI4'] = newDRscore - self.baseDRscore
        self.epanetmodel = None
        return PIs #, self.baseDemand_m3, leakDemand_m3, leakvolume

    def PI_step(self, Pmin: Optional[int] = 3, Plow: Optional[int] = 15):  # lost service time (pressure<3m)
        PI1 = 0
        PI2 = 0
        PI3_leakdemand = 0
        for node in self.node_pop:
            PI3_leakdemand = PI3_leakdemand + en.getnodevalue(self.epanetmodel.proj_for_simulation, node['index'], en.DEMAND)
            if node['pop'] > 0:
                pressure = en.getnodevalue(self.epanetmodel.proj_for_simulation, node['index'], en.PRESSURE)
                if pressure < Pmin:
                    PI1 = PI1 + node['pop']
                if pressure < Plow:
                    PI2 = PI2 + node['pop']
        return PI1, PI2, PI3_leakdemand
