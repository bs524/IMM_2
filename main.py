import epanet_fiware.epanetmodel
import epanet_fiware.enumerations as enu
import epanet_fiware.epanet_outfile_handler as outfile_handler
import epanet.toolkit as en
import pandas as pd
import numpy as np
import datetime
from typing import Optional
import multiprocess as mp
import IMM_PI

#%%Set-up IMM class
np.random.seed()

inp_file = 'C:/Users/bs524/OneDrive - University of Exeter/Documents/Exeter/dev/packages/anomaly-detection/data/gt/309D07_DMA_wgs84_rev4b.inp'
network_name = 'gt_DMA'

sensors_0 = {'ID': 'Moortown_SR.3092019_7230.1','Type': 'flow'}
sensors_1 = {'ID': '3092019_2290.3092019_2348.1','Type': 'flow'}
sensors_2 = {'ID': '3092019_7481','Type': 'pressure'}
sensors_3 = {'ID': '3092019_2136','Type': 'pressure'}
sensors_4 = {'ID': '3092019_2604','Type': 'pressure'}
sensors_5 = {'ID': '3092019_3276','Type': 'pressure'}
sensors_6 = {'ID': '3092019_2291','Type': 'pressure'}
sensors = [sensors_0, sensors_1, sensors_2, sensors_3 ,sensors_4,sensors_5, sensors_6]

closed_pipe_ids = ['TestPipe']
test = IMM_PI.IMM_model(inp_file=inp_file, network_name=network_name, sensors=sensors)
#%% Declare leaknodeID, LeakPipeID, awarenesstime, etc.
test.set_simulation_Parameters(leakNodeID = '3092019_10352',
                               leakPipeID='3092019_2256.3092019_2258.1',
                               OperationalPipeIDs = closed_pipe_ids)
#%% calc pop per node
test.get_pop_per_node(total_population = 6053)
#%% calc base demand and base velocity scores 48 hours after awarenesstime
test.base_scenario_sim()

#%%calc PIs for default pipe repair-leak scenario (i.e. repair pipe at time of awareness)
test.default_repair_scenario() #need to double check this

#%%Calc Objective function
#input: only the X vector
#run and calc PIS by compairing to PIS from default repair scenario
#x array:
# x0: repair start time (0-48)
# x1: closed pipe 1: opening time (0-48)
# x2: closed pipe 1: closing time (0-48)

x=np.array([0,1,47])
test.assess_IMM(x)

#%% GA Set-up
import numpy as np
from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling
from pymoo.optimize import minimize
from pymoo.core.problem import ElementwiseProblem
#from pymoo.core.problem import Problem

NUM_closed_pipes = 1
NUM_closed_pipe_constraints = 2*NUM_closed_pipes
NUM_PipeRepair_constraints = 1
repairDuration = 4 #pipe repair duration assumed to be 4hrs

class MyProblem(ElementwiseProblem):
    # Definition of a custom Knapsack problem
    def __init__(self):
        super().__init__(n_var = 1+NUM_closed_pipes*2,
                         n_obj = 1,
                         n_constr = NUM_PipeRepair_constraints + NUM_closed_pipe_constraints,
                         xl = 0,
                         xu = 48,
                         type_var = int
                         )

    def _evaluate(self, X, out, *args, **kwargs):
        # Objective and Constraint functions
        out["F"] = -test.assess_IMM(X)# Objective Value
        #pipe_repair_1 = -X[0]+0.5 #<= 0 #pipe repair time must be greater than 0
        pipe_repair_2 = X[0]+repairDuration-48 #<= 0 #pipe must be repaired within 48 hrs
        closedPipe1_1 = -X[1]*100 + X[2] #<=0 #if pipe 1 start time = 0, then pipe 1 endtime = 0
        closedPipe1_2 = X[1] - X[2] #<= 0 #closing time must occur > opening time
        out["G"] = np.column_stack([pipe_repair_2, closedPipe1_1, closedPipe1_2])


#%% Solution
start_time = datetime.datetime.now()

method = get_algorithm("ga",
                       pop_size=6,
                       sampling=get_sampling("int_random"),
                       crossover=get_crossover("int_sbx", prob=1.0, eta=3.0),
                       mutation=get_mutation("int_pm", eta=3.0),
                       eliminate_duplicates=True,
                       )

res = minimize(MyProblem(),
               method,
               termination=('n_gen', 11),
               seed=1,
               save_history=True
               )

print("Best solution found: %s" % res.X)
print("Function value: %s" % res.F)
print("Constraint violation: %s" % res.CV)

simulationTime = datetime.datetime.now() - start_time
print("Total training time: " + str(simulationTime))


#%%Parallelized - doesn't work
# from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling
# from pymoo.optimize import minimize
# from pymoo.core.problem import ElementwiseProblem
# from pymoo.core.problem import starmap_parallelized_eval
# from multiprocessing.pool import ThreadPool
# import multiprocessing
#
#
#
# NUM_closed_pipes = 1
# NUM_closed_pipe_constraints = 2*NUM_closed_pipes
# NUM_PipeRepair_constraints = 2
# repairDuration = 4 #pipe repair duration assumed to be 4hrs
#
#
#
# # the number of processes to be used
# n_proccess = 4
# pool = multiprocessing.Pool(n_proccess)
#
#
# class MyProblem(ElementwiseProblem):
#     def __init__(self):
#         super().__init__(n_var = 1+NUM_closed_pipes*2,
#                          n_obj = 1,
#                          n_constr = NUM_PipeRepair_constraints + NUM_closed_pipe_constraints,
#                          xl = 0,
#                          xu = 48,
#                          type_var = int,
#                          runner=pool.starmap,
#                          func_eval = starmap_parallelized_eval
#                          )
#
#     def _evaluate(self, X, out, *args, **kwargs):
#         # Objective and Constraint functions
#         out["F"] = -test.assess_IMM(X)# Objective Value
#         pipe_repair_1 = -X[0]+0.5 #<= 0 #pipe repair time must be greater than 0
#         pipe_repair_2 = X[0]+repairDuration-48 #<= 0 #pipe must be repaired within 48 hrs
#         closedPipe1_1 = -X[1]*100 + X[2] #<=0 #if pipe 1 start time = 0, then pipe 1 endtime = 0
#         closedPipe1_2 = X[1] - X[2] #<= 0 #closing time must occur > opening time
#         out["G"] = np.column_stack([pipe_repair_1, pipe_repair_2, closedPipe1_1, closedPipe1_2])
#
#
# #%%
#
# #solve solution
# method = get_algorithm("ga",
#                        pop_size=5,
#                        sampling=get_sampling("int_random"),
#                        crossover=get_crossover("int_sbx", prob=1.0, eta=3.0),
#                        mutation=get_mutation("int_pm", eta=3.0),
#                        eliminate_duplicates=True,
#                        )
#
# res = minimize(MyProblem(),
#                method,
#                termination=('n_gen', 5),
#                seed=1,
#                save_history=True
#                )
#
# pool.close()
# print("Best solution found: %s" % res.X)
# print("Function value: %s" % res.F)
# print("Constraint violation: %s" % res.CV)
#%% Visualization of Convergence

import matplotlib.pyplot as plt
# number of evaluations in each generation
n_evals = np.array([e.evaluator.n_eval for e in res.history])
# optimum value in each generation
opt = np.array([e.opt[0].F for e in res.history])

plt.title("Convergence")
plt.plot(n_evals, opt, "--")
plt.show()