import numpy as np
from pymoo.factory import get_algorithm, get_crossover, get_mutation, get_sampling
from pymoo.optimize import minimize
from pymoo.core.problem import Problem
#%% Problem Definiton
import main

NUM_closed_pipes = 1
NUM_closed_pipe_constraints = 2*NUM_closed_pipes
NUM_PipeRepair_constraints = 2
repairDuration = 4 #pipe repair duration assumed to be 4hrs

class MyProblem(Problem):
    # Definition of a custom Knapsack problem
    def __init__(self):
        super().__init__(n_var = 1+NUM_closed_pipes*2,
                         n_obj = 1,
                         n_constr = NUM_PipeRepair_constraints + NUM_closed_pipe_constraints,
                         xl = 0,
                         xu = 48,
                         type_var = int
                         )

    def _evaluate(self, X, out, *args, **kwargs):
        # Objective and Constraint functions
        out["F"] = -test.assess_IMM(X)# Objective Value
        pipe_repair_1 = -np.sum(X*[1,0,0], axis=1) +0.5<= 0 #pipe repair time must be greater than 0
        pipe_repair_2 = np.sum(X*[1,0,0], axis=1)+repairDuration  - 48<= 0 #pipe must be repaired within 48 hrs
        closedPipe1_1 = -np.sum(X*[0,1,0], axis=1)*100 + np.sum(X*[0,0,1], axis=1)<=0 #if pipe 1 start time = 0, then pipe 1 endtime = 0
        closedPipe1_2 = np.sum(X*[0,1,0], axis=1) - np.sum(X*[0,0,1], axis=1) <= 0 #closing time must occur > opening time
        out["G"] = np.column_stack([pipe_repair_1, pipe_repair_2, closedPipe1_1, closedPipe1_2])


#%% Solution
method = get_algorithm("ga",
                       pop_size=20,
                       sampling=get_sampling("int_random"),
                       crossover=get_crossover("int_sbx", prob=1.0, eta=3.0),
                       mutation=get_mutation("int_pm", eta=3.0),
                       eliminate_duplicates=True,
                       )

res = minimize(MyProblem(),
               method,
               termination=('n_gen', 30),
               seed=1,
               save_history=True
               )

print("Best solution found: %s" % res.X)
print("Function value: %s" % res.F)
print("Constraint violation: %s" % res.CV)

#%% Visualization of Convergence

import matplotlib.pyplot as plt
# number of evaluations in each generation
n_evals = np.array([e.evaluator.n_eval for e in res.history])
# optimum value in each generation
opt = np.array([e.opt[0].F for e in res.history])

plt.title("Convergence")
plt.plot(n_evals, opt, "--")
plt.show()